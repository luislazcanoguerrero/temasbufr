# Secuencia de descriptores para codificar en clave BUFR los días con heladas (T° < 0.0 °C) del mes
DATASUBSET 1
001001 85
001002 406
001015 "ARICA CHILE"
004001 2023
103000 {Descriptor de repeticiones}
031001 2
004002 1
004003 15
012101 301.2
004002 4
004003 35
012101 301.2

