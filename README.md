# TemasBufr

INSTALAR DOCKER y DOCKER COMPOSE PARA LINUX MINT 21 - UBUNTU
--------------------------------------------------------------------------------------
sudo apt update
sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/docker-archive-keyring.gpg
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"
sudo apt update
sudo apt -y install docker-ce
sudo usermod -aG docker $USER
newgrp docker
docker --version
docker compose version


CARPETAS
---------------------------------------------------------------
ExtractosManuales   : Secciones de interés del manual de clave
Lectura             : Temas y manuales variados sobre BUFR
Tablas              : Tablas específicas de BUFR
mensajes            : Carpeta enlazada a equipo host, deje sus mensajes codificados en esta carpeta


ARCHVIOS
--------------------------------------------------------------
Dockerfile          : Archivo definición de la imágen DOCKER
docker-compose.yml  : Definición del servicio
login               : shell para ingresar al equipo virtualizado
borra               : Elimina todos los contenedores e imágenes (!Cuidado!)
README.md           : Esta ayuda


COMANDOS
----------------------------------------------------------------------------------------------------
1. docker compose up -d     : Descarga, configura y crea la imágen y container del servicio
2. docker compose stop      : Detiene el servicio
3. docker compose start     : Inicia el servicio
4. ./login
5. ./borra


